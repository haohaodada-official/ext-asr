# ASR-THREE 离线语音识别模块


![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
  * [ArduinoC](#ArduinoC)
  * [MicroPython](#MicroPython)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)

## 相关链接
* 本项目加载链接: ```https://github.com/haohaodada-official/ext-asr```

* 用户库教程链接: ```https://mindplus.dfrobot.com.cn/extensions-user```


## 描述
支持好搭MIDI模块,可以播放各种MIDI音。

## 积木列表

![](./arduinoC/_images/blocks.png)



## 示例程序

### ArduinoC

![](./arduinoC/_images/example.png)

### MicroPython

![](./micropython/_images/example.png)



## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
arduino        |             |        √      |             | 
arduinonano        |             |       √       |             | 
mpython        |             |        √      |        √      | 
leonardo        |             |        √      |             | 
mega2560        |             |        √      |             | 


## 更新日志
* V0.0.1  基础功能完成
