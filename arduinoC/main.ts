//% color="#17959d" iconWidth=50 iconHeight=40
namespace ASR {
    //% block="initialize ASR pin#[PIN]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN" 
    export function set(parameter: any, block: any) {
        let pin=parameter.PIN.code;
        if (Generator.board === 'esp32') {//如果是掌控板，生成如下代码
            Generator.addSetup("GTSerialSetup",`Serial2.begin(115200 , ${pin}, P10);`);
        } else {
            let pin0;
            if (Generator.board === 'mega2560') {
                pin0=70;
            } else {
                pin0=21;
            }
            Generator.addInclude("addIncludeSoftwareSerial","#include <SoftwareSerial.h>");
            Generator.addObject("addasr","SoftwareSerial",`asr(${pin}, ${pin0});`);
            Generator.addSetup("GTSerialSetup",`asr.begin(115200);`);
        }
    }

    //% block="Is there ASR data to read?" blockType="boolean"
    export function available() {
        if (Generator.board === 'esp32'){//如果是掌控板，生成如下代码
            Generator.addCode(`Serial2.available()`)
        } else {
            Generator.addCode(`asr.available()`)
        }
    }

    //% block="read ASR data" blockType="reporter"
    export function read() {
        if (Generator.board === 'esp32'){//如果是掌控板，生成如下代码
            Generator.addCode(`Serial2.read()`)
        } else {
            Generator.addCode(`asr.read()`)
        }
    }
}
