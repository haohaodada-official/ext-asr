//% color="#17959d" iconWidth=50 iconHeight=40
namespace ASR {
    //% block="initialize ASR pin#[PIN]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN" 
    export function set(parameter: any, block: any) {
        let pin=parameter.PIN.code;
        Generator.addImport("from mpython import *");
        Generator.addImport("from machine import UART");
        Generator.addDeclaration("GTSerialSetup",`asr = UART(1, baudrate=115200, tx=Pin.P6, rx=Pin.${pin})`);
    }

    //% block="Is there ASR data to read?" blockType="boolean"
    export function any() {
        Generator.addCode(`asr.any()`)
    }

    //% block="read ASR data" blockType="reporter"
    export function read() {
        Generator.addCode(`ord(asr.read())`)
    }
}
